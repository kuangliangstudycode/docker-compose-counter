# docker-compose 实战之计数器


#### 介绍
Docker 学习之 docker-compose 容器编排 
自己创建一个 springboot 项目 ，模仿 docker 官网，做个利用 redis , 做个计数器

#### 软件架构
Springboot 框架  
Docker 容器

#### 安装教程

1.  需要安装 Docker 和 Docker Compose


#### 使用说明
**Step1**：创建springboot 项目 docker-compose

**Step2:**  编写 计算器 Controller 及 redis 相关配置

**Step3**:  编写 Dockerfile 文件 docker-compose.yml 文件

**Step4**:   将项目的 jar 包及 Dockerfile  docker-compose.yml 上传到服务器

**Step5：** docker-compose up 运行  ，然后用 docker-compose ps 查看服务

**Step6:** 访问 http://8.140.154.151:8080/arithmometer/hello   可以看到每次访问都会加1 
![输入图片说明](https://images.gitee.com/uploads/images/2021/0719/174113_d387aa20_8959076.png "屏幕截图.png")




#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
