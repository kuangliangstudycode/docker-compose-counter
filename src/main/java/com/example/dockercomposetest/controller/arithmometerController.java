package com.example.dockercomposetest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName arithmometerController
 * @Description 计数器 Controller
 * @Author LiuYi
 * @Date 2021/7/18  11:40
 * @Version 1.0
 */
@RestController
@RequestMapping("/arithmometer")
public class arithmometerController {

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @GetMapping("/hello")
    public String test(){
        Long views = stringRedisTemplate.opsForValue().increment("views");
        return "Hello World! I have been seen {"+views+"} times.";
    }
}
